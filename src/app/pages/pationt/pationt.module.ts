import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PationtRoutingModule } from './pationt-routing.module';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditComponent } from './edit/edit.component';


@NgModule({
  declarations: [
    AddComponent,
    ListComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    PationtRoutingModule,
    ReactiveFormsModule
  ],
  exports:[
    AddComponent,
    ListComponent,
    EditComponent
  ]
})
export class PationtModule { }
