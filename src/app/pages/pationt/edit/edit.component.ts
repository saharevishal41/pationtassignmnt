import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pationt, pationtC } from 'src/app/config/pationt/pationt';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
public pationtArr:Pationt[]=[];
public pationtObj:pationtC=new pationtC();
  constructor(private route:ActivatedRoute){
    
  }
  ngOnInit(): void {
    
      const localData=localStorage.getItem('pationtData')
    if(localData !=null){
      this.pationtArr= JSON.parse(localData)
    }  
    console.log(this.pationtArr);
    
    this.route.snapshot.params[2]
    console.log(this.route.snapshot.params[2]);
    
  }
  
}
