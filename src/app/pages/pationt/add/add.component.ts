import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Pationt, pationtC } from 'src/app/config/pationt/pationt';
import { PationtService } from 'src/app/config/pationt/pationt.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit{
public form:FormGroup
public pationtArr:Pationt[]=[];
public pationtObj:pationtC=new pationtC()
private newId?:number
constructor(private fb:FormBuilder,private pationtSRV:PationtService){
this.form=this.fb.group({
  id:[this.newId],
  fname:['' ,[Validators.required]],
  lname:['' ,[Validators.required]],
  mobileNo:['' ,[Validators.required]],
  email:['' ,[Validators.required]],
  address1:['' ,[Validators.required]],
  address2:['' ,[Validators.required]],
  city:['' ,[Validators.required]],
  state:['' ,[Validators.required]],
  zipcode:['' ,[Validators.required]],
})
}
// this.form=new FormGroup({
//   id:new FormControl(''),
//     fname:new FormControl(''),
//     lname:new FormControl(''),
//     mobileNo:new FormControl(''),
//     email:new FormControl(''),
//     address1:new FormControl(''),
//     address2:new FormControl(''),
//     city:new FormControl(''),
//     state:new FormControl(''),
//     zipcode:new FormControl(''),
// })
// }
ngOnInit(): void {
  const localData=localStorage.getItem('pationtsData')
  if(localData !=null){
    this.pationtArr= JSON.parse(localData)
  }
}

get id(){
  return this.form.get('id')
}
get fname(){
  return this.form.get('fname')
}
get lname(){
  return this.form.get('lname')
}
get mobileNo(){
  return this.form.get('mobileNo')
}
get email(){
  return this.form.get('email')
}
get address1(){
  return this.form.get('address1')
}
get address2(){
  return this.form.get('address2')
}
get city(){
  return this.form.get('city')
}
get state(){
  return this.form.get('state')
}
get zipcode(){
  return this.form.get('zipcode')
}


errorMSG={
  fname:[
    {
      type:'required', message:'Enter First name '
    }
  ],
  lname:[
    {
      type:'required', message:'Enter First name '
    }
  ],
  mobileNo:[
    {
      type:'required', message:'Enter First name '
    }
  ],
  email:[
    {
      type:'required', message:'Enter First name '
    }
  ],
  address1:[
    {
      type:'required', message:'Enter First name '
    }
  ],
  address2:[
    {
      type:'required', message:'Enter First name '
    }
  ],
  city:[
    {
      type:'required', message:'Enter First name '
    }
  ],
  state:[
    {
      type:'required', message:'Enter First name '
    }
  ],
  zipcode:[
    {
      type:'required', message:'Enter First name '
    }
  ],
}


// add function
save(){

if(this.form.valid){
  // debugger
  const pationtdata=this.form.value
  pationtdata.id=this.pationtArr.length+1
  console.log(this.pationtObj);
 this.pationtArr.push(this.form.value)
  localStorage.setItem('pationtsData',JSON.stringify(this.pationtArr))
 console.log(this.pationtArr);
 this.form.reset()
 
}else{
  this.form.markAllAsTouched()
  this.form.markAsTouched()
}
}


}
