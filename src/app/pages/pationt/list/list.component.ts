import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pationt, pationtC } from 'src/app/config/pationt/pationt';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit,AfterViewInit{
  @Input() pationtArr!: Pationt[];
  
public pationtObj:pationtC=new pationtC()

constructor(private route:ActivatedRoute,private router:Router){

}
  ngOnInit(): void {
    // const localData=localStorage.getItem('pationtData')
    // if(localData !=null){
    //   this.pationtArr= JSON.parse(localData)
    // }  
      
  }
  ngAfterViewInit(): void {
   
  }
 

  editData(id:number){
    // this.route.snapshot.params[id]
    this.router.navigateByUrl('pationt/'+id)
const currentRecord=this.pationtArr.find(m=>m.id==id)
if(currentRecord !=undefined){
  console.log(currentRecord);
  
}
  }

  deleteData(id:number){
    const currentIndex=this.pationtArr.findIndex(m=>m.id==id)
    this.pationtArr.splice(currentIndex,1)
    localStorage.setItem('pationtsData',JSON.stringify(this.pationtArr))
  }

  updateItem(index: number, item: any): void {
    // let items = this.getItems();
    // items[index] = item;
    // this.localStorage.set('items', items);
    // this.items = items;

debugger
    this.pationtArr[index]=item;
    localStorage.setItem('pationtsData',JSON.stringify(item))
  }
  
}
