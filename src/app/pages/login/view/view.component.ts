import { Component } from '@angular/core';
import{FormGroup,FormBuilder,Validators}from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent {
public form:FormGroup;
constructor(private fb:FormBuilder,private router:Router){
  this.form=this.fb.group({
    email:['',[Validators.required]],
    password:['',[Validators.required]],
    reminder:['']
  })

}
login(){
  if(this.form.valid){
    console.log(this.form.value);
    this.router.navigateByUrl('/pationt/add')
  }
  else{
    this.form.markAsUntouched();
    this.form.markAllAsTouched()
  } 
}


get email() {
  return this.form.get('email')
}
get password() {
  return this.form.get('password')
}


public errorMessages={
  email:[
    { type: 'required', message: 'email name is required' },
   
  ],
  password:[
    { type: 'required', message: 'email name is required' },
   
  ],
}
}
