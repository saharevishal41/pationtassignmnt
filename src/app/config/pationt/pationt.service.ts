import { Injectable } from '@angular/core';
import { Pationt } from './pationt';

@Injectable({
  providedIn: 'root'
})
export class PationtService {
  patients: Pationt[]=[];

  getPatients(): Pationt[] {
    const patientsData = localStorage.getItem('pationtsData');
    if (patientsData) {
      this.patients = JSON.parse(patientsData);
    }
    return this.patients;
  }

  addPatient(patient: Pationt): void {
    this.patients.push(patient);
    // Save patients to localStorage
    localStorage.setItem('pationtsData', JSON.stringify(this.patients));
  }

  updatePatient(patient: Pationt): void {
    const index = this.patients.findIndex(p => p.id === patient.id);
    if (index !== -1) {
      this.patients[index] = patient;
      // Save patients to localStorage
      localStorage.setItem('pationtsData', JSON.stringify(this.patients));
    }
  }

  deletePatient(id: number): void {
    const index = this.patients.findIndex((p:Pationt) => p.id === id);
    if (index !== -1) {
      this.patients.splice(index, 1);
      // Save patients to localStorage
      localStorage.setItem('pationtsData', JSON.stringify(this.patients));
    }
  }
}
