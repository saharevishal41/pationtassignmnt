import { TestBed } from '@angular/core/testing';

import { PationtService } from './pationt.service';

describe('PationtService', () => {
  let service: PationtService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PationtService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
